## Строение проекта и вводная информация

```
\Local\Core
    \Agent - точка входа всех агентов, передают данные на обработчики
    \Ajax
        \Handler - обработчики всех ajax
    \Assistant - ассистенты, упрощают жизнь разработчику
    \Console - пространство для работы через консоли
        \Command - хранилище комманд, передает выполнение в раннер
        \Runner - раннер, содержит логику обработки запроса
    \EventHandlers - простарнство обработчиков bitrix
    \Inner - пространство внутренних модулей, классов и обработчиков
        \Admin - системное постранство для админки
        \AdminHelper - хелперы для системы отображения в админке
            \Data & \Reference - пронстранства страниц списков и деталок для 
                ORM моделей в админке
        \Agent - обработчики агентов
        \BxModified - пространство модифицированных битриксовых классов
        \Client - пространство для клиентов
        \TriggerMail - пространство обработчиков почтовых событий
    \Model - пространство ORM моделей
        \Data - пространство рядовых данных
        \Reference - пространство справочиников
```

## API | EASY

### \Local\Core\Model

Пространство, отвечающее за работу с ORM. Для получения подробной информации изучи README.md, размещенный в этом этом пространстве.

---
### \Local\Core\Inner\AdminHelper
Для создания своих сраниц от модуля в админке необходимо сделать новый класс в **Local\Core\Inner\AdminHelper\Data**, если это используется **ORM**, либо другой, если появится такая нужда.
 
 Страница списка наследуетс от **\Local\Core\Inner\AdminHelper\EditBase**, деталки - от **\Local\Core\Inner\AdminHelper\ListBase** . Пример реализации можно глянуть в **Local\Core\Inner\AdminHelper\Data\Company** .
 
 **Все классы админки должны лежать в одном месте, в \Local\Core\Inner\AdminHelper !**

После создания класса его необходимо зарегистрировать в **local/modules/local.core/admin/admin_helper_route.php** и **local/modules/local.core/admin/menu.php** . Пример там же, все понятно.

Расписывать подробно что да как работает нет нужны - просто пройдись по файлам и пространствам, перечисленным выше и сделай по аналогии. Первый раз будет сложно, но после первого раза все становится очевидным.

---
### \Local\Core\Assistant
Это область ассистента. В ней предполагается хранение классов-ассистентов, которые помогают быстро получить какие либо данные. К примеру **\Local\Core\Assistant\Iblock\Iblock::getIdByCode()** помогает получить ID ИБ по его коду и коду Типа ИБ, в котором он находится. При этом результат ассистента должен быть короткий, оптимизированный и либо кешироваться, либо записываться в регистр класса (на примере **\Local\Core\Assistant\HighLoadBlock\HighLoadBlock::getEntity()**)

---
### \Local\Core\Agent\Base
Все агенты должны быть унаследованы от **\Local\Core\Agent\Base**.
Вызов агента происходит путем вызова **::init()**

Для примера и понимания смотри **\Local\Core\Agent\Example\ExampleAgent**.

Для простоты создания агента используй **\CLocalCore::addAgent()**

---
### \Local\Core\EventHandlers\Base
Все обработчики должны быть инициализированы в **\Local\Core\EventHandlers\Base** и располагаться в методе, который назвывается как модуль, к котому относится обработчик.
Пример:
```php
private static function registerMain()
{
    $eventManager = EventManager::getInstance();

    /** @see \Local\Core\EventHandlers\Main\OnBeforeProlog::initializeRegionHost() */
    $eventManager->addEventHandler('main', 'OnBeforeProlog', [Main\OnBeforeProlog::class, 'initializeRegionHost']);

}
```
Для большего понимания смотри сам **\Local\Core\EventHandlers\Base**

Размещать сами обработчики необходимо в пространстве из модулей. В качестве класса выступает название обработчика. Лучше всего идею отображается в нвазнии и пространстве класса **\Local\Core\EventHandlers\Iblock\OnIBlockPropertyBuildList** 

---
### \Local\Core\Inner\BxModified
Данная директория предназначена для размещения классов, которые являются битрисковыми, но требовали доработки с нашей стороны. К примеру вместо **\CFile** необходимо теперь использовать **\Local\Core\Inner\BxModified\CFile**. 

Если класс тащит за собой цепочку других рядом стоящих классов, то их необходимо вынести и сгрупировать в подкатегорию по названию класса. К примеру **\Local\Core\Inner\BxModified\CFile\CFile**

Список таких классов постоянно пополняется, необходимо за ним следить.

---
### Компоненты

Все компонента local.core должны быть реализованы через класс и быть унаследованы от **\Local\Core\Inner\BxModified\CBitrixComponent**.

**\Local\Core\Inner\BxModified\CBitrixComponent** содержите изначально определенные методы:
+ _show404Page() - моментально прекращает работу компонента и выводит 404

По мере разработки проекта в него могут вноситься изменения и модифицироваться логика, поэтому наследуемся строго от этого класса.

---
### \Local\Core\Inner\Route
Класс рассчитан на работу с путями по единому шаблону. 
Для корректной работы в корне сайта требуется создать файл **.routerewrite.php** и объявить внутри массив **$arLocalRoutes**. 
##### Структура массива и пример его реализации: 
```php
$arLocalRoutes = [
    'company' => [
        'list'   => [
            'URL'         => '/personal/company/',
            'BREADCRUMBS' => function($arParams = [])
                {
                    $GLOBALS['APPLICATION']->AddChainItem(
                        "Мои компании",
                        \Local\Core\Inner\Route::getRouteTo(
                            'company',
                            'list'
                        )
                    );
                }
        ],
        'edit'   => [
            'URL'         => '/personal/company/#COMPANY_ID#/edit/',
            'BREADCRUMBS' => function($arParams = [])
                {
                    \Local\Core\Inner\Route::fillRouteBreadcrumbs(
                        'company',
                        'list'
                    );
                    $GLOBALS['APPLICATION']->AddChainItem('Редактирование компании '.\Local\Core\Inner\Company\Base::getCompanyName($arParams['COMPANY_ID']));
                }
        ],
    ],
    'store' => [
        'list' => [
            'URL' => '/personal/company/#COMPANY_ID#/store/',
            'BREADCRUMBS' => function ($arParams = [])
                {
                    \Local\Core\Inner\Route::fillRouteBreadcrumbs('company', 'detail', ['COMPANY_ID' => $arParams['COMPANY_ID']]);
                    $GLOBALS['APPLICATION']->AddChainItem("Магазины", \Local\Core\Inner\Route::getRouteTo('store', 'list', ['COMPANY_ID' => $arParams['COMPANY_ID']]));
                }
        ],
        'detail' => [
            'URL' => '/personal/company/#COMPANY_ID#/store/#STORE_ID#/',
            'BREADCRUMBS' => function ($arParams = [])
                {
                    \Local\Core\Inner\Route::fillRouteBreadcrumbs('store', 'list', ['COMPANY_ID' => $arParams['COMPANY_ID']]);

                    $GLOBALS['APPLICATION']->AddChainItem(\Local\Core\Inner\Store\Base::getStoreName($arParams['STORE_ID']),
                        \Local\Core\Inner\Route::getRouteTo('store', 'detail', ['COMPANY_ID' => $arParams['COMPANY_ID'], 'STORE_ID' => $arParams['STORE_ID']]));
                }
        ],
    ],
];
```
В данном примере **company** - это ключ **$strDirectory**, **list** - **$strAction**.
Экшен - массив со следующими ключами:
+ URL - нужен для **getRouteTo()**, записывается урл, возможно размещение плэйсхолдера.
+ BREADCRUMBS - каллбек функция, нужна для **fillRouteBreadcrumbs()**, имеет параметр **$arParams**, в который передаютс параметры. В этой функции происходит построение цепочки навигации. Предпочтительно использовать анонимные функции. Использовать на свое усмотрение, ключ не обязательный.

##### \Local\Core\Inner\Route::getRouteTo
Возвращает путь по роуту, заменяя плейсхолдеры.

Пример вызова с заменой плейсхолдеров из структуры выше:
```php
\Local\Core\Inner\Route::getRouteTo('company','list');
// /personal/company/


\Local\Core\Inner\Route::getRouteTo('company','edit', ['COMPANY_ID' => 12]);
// /personal/company/12/edit/
```

##### \Local\Core\Inner\Route::fillRouteBreadcrumbs
Заполняет breadcrumbs по роуту.

Пример вызова:
```php
\Local\Core\Inner\Route::fillRouteBreadcrumbs('company', 'list');

\Local\Core\Inner\Route::fillRouteBreadcrumbs('company', 'edit', ['COMPANY_ID' => $arParams['COMPANY_ID']]);
```
Предполагается вызывать его из **component_epilog.php**

---

### Ajax и \Local\Core\Ajax\Handler
Все ajax отправляются на **/ajax/**. Для работы с ajax следует использовать библиотеку **axios** ввиду ее легковесности, удобства настройки и Promise.

В **/ajax/routes/routes.php** следует настроить правило обработки роута. По текущим реализациям там все понятно. Отправляя ajax надо передавать sessid. По умолчанию в axios его передача настроена, поэтому вписывать его каждый раз не имеет нужды.

Настроив роут в /ajax/routes/routes.php нужный путь следует разместить обработчик в **\Local\Core\Ajax\Handler** . Вызываемые методы должны быть статичны и публичны.

Для пример изучи **\Local\Core\Ajax\Handler\Example**.

Пример обработчика:
```php
public static function delete(\Bitrix\Main\HttpRequest $oRequest, \Local\Core\Inner\BxModified\HttpResponse $oResponse, $args)
{
    $arResult = [];

    if( !empty($args['company_id']) )
    {
        $intCompanyId = $args['company_id'];

        $ar = CompanyTable::getByPrimary($intCompanyId, ['filter' => ['USER_OWN_ID' => $GLOBALS['USER']->GetId()], 'select' => ['ID']])->fetch();
        if( !empty($ar['ID']) )
        {
            CompanyTable::delete($ar['ID']);
            $arResult['result'] = 'SUCCESS';
        }
    }

    if( empty($arResult['result']) )
    {
        $arResult['result'] = 'error';
    }

    $oResponse->setContentJson($arResult);
}
```
Как видно из примера единственный способ отдать информацию из обработчика - это передача в **$response**. Из этого можно сделать вывод, что **все ответы ajax должен отдавать в формате JSON**, которые следует обработать на входе.

Пример функции, которая отправляет запрос ajax и обрабатывает его
```js
<script type="text/javascript">
    var data = {
        "qq": "aa",
        "ww": "rr"
    },
    intId = 12;
    axios.post('/ajax/company/delete/'+intId+'/', qs.stringify(data))
        .then(function (response) {
             if( response.data.result == 'SUCCESS' )
             {
                 alert('OK!');
             }
             else
             {
                 alert('ERROR!')
             }
        })
        .then(function (catchResponse) {
             console.log(catchResponse);
        })
</script>
```

---
### Условные операторы в почтовых шаблонах
Пример почтового шаблона
```
У магазина "#STORE_NAME#" изменился тарифный план.<br>
Новый тариф - "#NEW_TARIFF_NAME#" с ограничением в #NEW_TARIFF_PRODUCT_LIMIT# товаров.
{{{IF #DATE_ACTIVE_TO#;!=;}}}
<br/>
Тариф действует до #DATE_ACTIVE_TO#. Далее он будет заменен на "#NEXT_TARIFF_NAME#".
{{{ENDIF}}}
```
По регулярке ищем вхождения, проверяем условия и исполяем их. Условие должно быть перечислено в блок **IF**. Дальше идет ЗНАЧЕНИЕ1;ОПЕРАТОР;ЗНАЧЕНИЕ2.

Это условие разбирается через **str_getcsv**, разделитель - **;**, если в значении должен быть **;** - обернуть значение в **"**. Все как в csv.
```
{{{IF #TEXT#;!=;"qq;qq"}}}
```

Доступно сравнение только 2х значений, каждое из них может быть placemark. Так же стоит понимать, что значения надо передавать уже в конечном виде, как и всегда. Для сравнения дат можно к примеру сравнивать их timestamp.
```
{{{IF #ACTIVE_FROM_TIMESTAMP#;<=;#ACTIVE_TO_TIMESTAMP#}}}
```
Доступные операторы: == ( можно использовать и = ), !=, >, >=, <, <=. 

**Нельзя использовать вложенные условия.**

---
## API | MEDIUM

### local/tools/console

Основной скрипт-контроллер всех консольных команд. Главным преимущесвтом использования модуля symfony/console является то, что многие рутинные задачи в ней уже решены и имеется возможность увидеть весь список доступных консольных команд. Для этого необходимо в папке скрипта выполнить команду (**local/tools/**):
```
php console list
```
Работает на базе Symfony/Console
 
Написать свою компанду можно, разместив ее в **\Local\Core\Console\Command** по примерну **\Local\Core\Console\Command\DemoConsole**.

---
### \Local\Core\Inner\Client

Во время разработки возникает нужда использовать сторонние сервисы. Будь то DaData, reCaptcha и т.п.
Все клиенты должны размещаться в **\Local\Core\Inner\Client**.

---
### \Local\Core\Inner\TriggerMail
Это пространство для отправки почтовых событий. Инициировать отправку событий следует через них, а не через \CEvent напрямую.

---
### \Local\Core\Inner\Facet\Reindex
Класс для запуска переиндексации фасетного индекса.
Пример закуска

```
$oOutput = new \Symfony\Component\Console\Output\ConsoleOutput();
(new \Local\Core\Inner\Facet\Reindex($oOutput))
    ->setIblockId(2) 
    ->setExecutionTimeSec(20)
    ->execute();
```
Предпочтительней использовать запуск из-под консоли
```
php -d short_open_tag=1 local/tools/console facet:reindex -vvv
```

---
### \Local\Core\Inner\Product\Product
Класс для работы с товаром. Через него необходимо получать данные по складам, статичные св-ва и статичные цены.

Примеры:
```
# Для одного товара
$oProduct = new \Local\Core\Inner\Product\Product(123);
$oProduct->isCanBuy(); // Вернут признак можно купить или нет
$oProduct->getTotalAmount();
$oProduct->getCachedPrice();
$oProduct->getCachedProperties();

# Для массива товаров
$oProduct = new \Local\Core\Inner\Product\Product([
    120,
    121,
    122,
    123
]);
# При работе с массивом товаров необходимо четко передать id товара.
# В противном случае будет выкинуто исключение.
$oProduct->isCanBuy(120); // Вернут признак можно купить или нет товара с id 120
$oProduct->getTotalAmount(120);
$oProduct->getCachedPrice(122);
$oProduct->getCachedProperties(123);
```
---
### \Local\Core\Inner\Catalog\Filter
Ввиду специфичности требований к выводу фильтров появился этот класс. 
Имеет лишь один метод. Принимает id типа изделия и выдает список символьных кодов фильтров, которые надо вывести.

```
\Local\Core\Inner\Catalog\Filter::getAllowedPropsByProductType(12);
```
