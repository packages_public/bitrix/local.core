<?php


namespace Local\Core\Agent\Example;

/**
 * Пример агента.
 *
 * @package Local\Core\Agent\Example
 */
class ExampleAgent extends \Local\Core\Agent\Base
{
    protected static function execute()
    {
        // Получим параметры, которые передавались в агента
        $aParams = func_get_args();

        /**
         * Далее вызываем метом, который отвечает за логику работы агента.
         * Методы должны располагаться в
         */
        \Local\Core\Inner\Agent\Example\ExampleAgent::run($aParams[0], $aParams[1]);
    }
}