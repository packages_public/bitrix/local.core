<?php

namespace Local\Core\Inner\AdminHelper\EditField;

class Dump extends Base
{

    /**
     * {@inheritdoc}
     */
    public function getEditFieldHtml()
    {
        ob_start();
        dump($this->getValue());
        $sDataFields = ob_get_contents();
        ob_end_clean();

        return $sDataFields;
    }

    /**
     * {@inheritdoc}
     */
    public function getViewFieldHtml()
    {
        ob_start();
        dump($this->getValue());
        $sDataFields = ob_get_contents();
        ob_end_clean();

        return $sDataFields;
    }

}
