<?php

namespace Local\Core\Inner\AdminHelper\EditField;

class Select extends Base
{
    protected $variants = [];

    protected $sDefText = 'Не выбрано';

    /**
     * {@inheritdoc}
     */
    public function getEditFieldHtml()
    {
        $variants = [
            "reference" => array_values($this->variants),
            "reference_id" => array_keys($this->variants),
        ];

        return SelectBoxFromArray($this->getCode(), $variants, $this->getValue(), $this->sDefText, "");
    }

    /**
     * {@inheritdoc}
     */
    public function getViewFieldHtml()
    {
        $result = "";

        if (!empty($this->getValue())) {

            if (isset($this->variants[$this->getValue()])) {

                $result = "{$this->variants[$this->getValue()]}[{$this->getValue()}]";
            }

        }

        return $result;
    }

    /**
     * Устанавливает варианты значений
     *
     * @param array $variants
     *
     * @return $this
     */
    public function setVariants(array $variants)
    {
        $this->variants = $variants;

        return $this;
    }

    /**
     * @param string $sDefText
     *
     * @return $this
     */
    public function setDefText(string $sDefText)
    {
        $this->sDefText = $sDefText;

        return $this;
    }

}
