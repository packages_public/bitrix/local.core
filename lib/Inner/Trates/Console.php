<?php

namespace Local\Core\Inner\Trates;

use \Symfony\Component\Console\Helper\ProgressBar;

/**
 * Трейт для работы с консолью
 *
 * @package Local\Core\Inner\Trates
 */
trait Console
{
    /** @var \Symfony\Component\Console\Output\OutputInterface $oOutput */
    protected $oOutput = null;


    /**
     * Вывод заголовка
     *
     * @param $sMsg
     */
    protected function printHeader($sMsg)
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            if ($this->oOutput->isDebug()) {
                $this->oOutput->writeln('<bg=yellow;fg=black;options=bold>'.$sMsg.'</>', true);
            }
        } else {
            dump($sMsg);
        }
    }

    /**
     * Вывод сообщения
     *
     * @param $sMsg
     */
    protected function printMessage($sMsg)
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            if ($this->oOutput->isDebug()) {
                $this->oOutput->writeln('<comment>'.$sMsg.'</comment>', true);
            }
        } else {
            dump($sMsg);
        }
    }

    /**
     * Вывод ошибки
     *
     * @param $sMsg
     */
    protected function printError($sMsg)
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            $this->oOutput->writeln('<error>'.$sMsg.'</error>', true);
        } else {
            dump($sMsg);
        }
    }

    /**
     * @param $sEntity
     * @param $sAction
     * @param $aData
     *
     * @throws \Exception
     */
    protected function addMessageToLog($sEntity, $sAction, $aData)
    {
        \Local\Core\Model\Data\ImportLogTable::add([
            'ENTITY' => $sEntity,
            'ACTION' => $sAction,
            'DATA' => $aData,
        ]);
    }

    /** @var ProgressBar $oProgressBar */
    protected $oProgressBar = null;


    /**
     * Начинает прогресс
     */
    protected function progressBarStart()
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            if ($this->oOutput->isDebug()) {
                $this->oProgressBar = new ProgressBar($this->oOutput, 0);
                $this->oProgressBar->setFormat('debug');
                $this->oProgressBar->setBarWidth(100);
                $this->oProgressBar->setBarCharacter('<comment>=</comment>');
                $this->oProgressBar->setEmptyBarCharacter(' ');
                $this->oProgressBar->setProgressCharacter('|');
                $this->oProgressBar->start();
            }
        }
    }

    /**
     * Заканчивает прогресс
     */
    protected function progressBarFinish()
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            if ($this->oOutput->isDebug()) {
                $this->oProgressBar->finish();
                $this->oOutput->writeln('', true);
            }
        }
    }

    /**
     * Сдвигает прогресс
     *
     * @param int $iStepNum
     */
    protected function progressBarAdvance($iStepNum = 1)
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            if ($this->oOutput->isDebug()) {
                $this->oProgressBar->advance($iStepNum);
            }
        }
    }

    /**
     * Увеличить прогресс на N шагов
     *
     * @param $iInt
     */
    protected function progressBarAddMaxSteps($iInt)
    {
        if ($this->oOutput instanceof \Symfony\Component\Console\Output\OutputInterface) {
            if ($this->oOutput->isDebug()) {
                $this->oProgressBar->setMaxSteps((int)$this->oProgressBar->getMaxSteps() + $iInt);
            }
        }
    }

}
