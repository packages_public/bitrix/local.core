<?php

namespace Local\Core\Inner\Facet;

use \Local\Core\Assistant\Iblock;

/**
 * Class Reindex
 * @package Local\Core\Inner\Facet
 */
class Reindex
{
    use \Local\Core\Inner\Trates\Console;

    /**
     * Filter constructor.
     *
     * @param \Symfony\Component\Console\Output\OutputInterface $oOutput
     */
    public function __construct(\Symfony\Component\Console\Output\OutputInterface $oOutput)
    {
        $this->oOutput = $oOutput;
    }

    protected $iIblockId = 0;
    protected $iExecutionTimeSec = 20;

    protected $iProductProcessed = 0;
    protected $iProductProcessedLastStep = 0;
    protected $iProductTotal = 0;
    protected $iProductLastId = 0;

    /** @var \Bitrix\Iblock\PropertyIndex\Indexer $oIndex */
    protected $oIndex = null;


    /**
     * @param int $iIblockId
     *
     * @return $this
     */
    public function setIblockId(int $iIblockId)
    {
        $this->iIblockId = $iIblockId;
        return $this;
    }

    /**
     * @param int $iExecutionTimeSec
     *
     * @return $this
     */
    public function setExecutionTimeSec(int $iExecutionTimeSec)
    {
        $this->iExecutionTimeSec = $iExecutionTimeSec;
        return $this;
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function execute()
    {
        if ($this->iIblockId < 1) {
            throw new \Exception('Set the IblockId');
        }
        if ($this->iExecutionTimeSec < 10) {
            throw new \Exception('Set the execution time more than 10 seconds');
        }

        if ($this->isNeedReindex()) {
            $this->reindex();
        }
    }

    /**
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function isNeedReindex()
    {
        $iblockInfo = \Bitrix\Iblock\IblockTable::getList(array(
            'select' => array('ID', 'PROPERTY_INDEX'),
            'filter' => array('=ID' => $this->iIblockId)
        ))
            ->fetch();

        return ($iblockInfo['PROPERTY_INDEX'] == 'I');
    }

    /**
     *
     */
    protected function reindex()
    {

        $this->oIndex = \Bitrix\Iblock\PropertyIndex\Manager::createIndexer($this->iIblockId);
        $this->oIndex->startIndex();
        $this->iProductTotal = $this->oIndex->estimateElementCount();

        if ($this->oOutput->isDebug()) {
            $this->progressBarStart();
            $this->progressBarAddMaxSteps($this->iProductTotal);
        }

        do
        {
            if ($this->oOutput->isDebug()) {
                $this->progressBarAdvance($this->iProductProcessedLastStep);
            }
        }
        while($this->reindexContinue());

        if ($this->oOutput->isDebug()) {
            $this->progressBarFinish();
        }
    }

    /**
     * @return bool
     */
    protected function reindexContinue()
    {
        $this->oIndex->setLastElementId($this->iProductLastId);

        $iIndexedProductCounts = $this->oIndex->continueIndex($this->iExecutionTimeSec);

        if ($iIndexedProductCounts > 0) {
            $this->iProductProcessedLastStep = $iIndexedProductCounts;
            $this->iProductProcessed += $iIndexedProductCounts;
            $this->iProductLastId = $this->oIndex->getLastElementId();
            return true;
        } else {
            $this->oIndex->endIndex();
            \CBitrixComponent::clearComponentCache("bitrix:catalog.smart.filter");
            \CIBlock::clearIblockTagCache($this->iIblockId);
            return false;
        }
    }
}
