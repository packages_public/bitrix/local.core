<?php


namespace Local\Core\Inner\Product;

/**
 * Class Product
 * @package Local\Core\Inner\Product
 */
class Product
{
    protected $mixProductId = null;

    /**
     * Product constructor.
     *
     * @param mixed $mixProductId ID товара или массив из ID
     */
    public function __construct($mixProductId)
    {
        $this->mixProductId = $mixProductId;
    }

    /**
     * Получить данные по складам
     *
     * @return array|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getStocks()
    {
        if (is_null($this->aProductStock)) {
            $this->fillProductStock();
        }
        return $this->aProductStock;
    }

    /**
     * Можно ли купить
     *
     * @param int $iProductId
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function isCanBuy($iProductId = null)
    {
        if (is_array($this->mixProductId) && is_null($iProductId)) {
            throw new \Exception('mixProductId must be integer or set $iProductId');
        }

        return $this->getTotalAmount($iProductId) > 0;
    }

    /**
     * Общее кол-во товаров по складам
     *
     * @param int $iProductId
     *
     * @return false|int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function getTotalAmount($iProductId = null)
    {
        if (is_array($this->mixProductId) && is_null($iProductId)) {
            throw new \Exception('mixProductId must be integer or set $iProductId');
        }

        if (is_null($this->aProductStock)) {
            $this->fillProductStock();
        }

        if (is_array($this->aProductStock)) {
            return (int)array_sum(array_column($this->aProductStock[$iProductId ?? $this->mixProductId], 'AMOUNT'));
        }

        return false;
    }

    protected $aProductStock = null;

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function fillProductStock()
    {
        $oProductStocks = \Local\Core\Model\Data\ProductStockTable::getList([
            'filter' => [
                'PRODUCT_ID' => $this->mixProductId
            ],
            'select' => [
                'PRODUCT_ID',
                'STOCK_ID',
                'AMOUNT',
            ]
        ]);
        while ($a = $oProductStocks->fetch()) {
            $this->aProductStock[$a['PRODUCT_ID']][] = [
                'STOCK_ID' => $a['STOCK_ID'],
                'AMOUNT' => $a['AMOUNT'],
            ];
        }
    }


    /**
     * @param      $aData
     * @param int  $iProductId
     *
     * @throws \Bitrix\Main\SystemException
     */
    public function setCachedProperties($aData, $iProductId = null)
    {
        if (is_array($this->mixProductId) && is_null($iProductId)) {
            throw new \Exception('mixProductId must be integer or set $iProductId');
        }

        BXClearCache(true, 'local.core/product/'.($iProductId ?? $this->mixProductId).'/propsFull');

        $oCache = \Bitrix\Main\Application::getInstance()
            ->getCache();
        if (
        $oCache->startDataCache(
            60 * 60 * 24 * 365,
            'PRODUCT_PROPS#FULL#'.($iProductId ?? $this->mixProductId),
            'local.core/product/'.($iProductId ?? $this->mixProductId).'/propsFull'
        )
        ) {
            $oCache->endDataCache($aData);
        }
    }

    /**
     * @param int $iProductId
     *
     * @return array|false
     * @throws \Bitrix\Main\SystemException
     */
    public function getCachedProperties($iProductId = null)
    {
        if (is_array($this->mixProductId) && is_null($iProductId)) {
            throw new \Exception('mixProductId must be integer or set $iProductId');
        }

        $oCache = \Bitrix\Main\Application::getInstance()
            ->getCache();

        if (
        $oCache->initCache(
            60 * 60 * 24 * 365,
            'PRODUCT_PROPS#FULL#'.($iProductId ?? $this->mixProductId),
            'local.core/product/'.($iProductId ?? $this->mixProductId).'/propsFull'
        )
        ) {
            return $oCache->getVars();
        }
        return false;
    }


    /**
     * @param float $fPrice
     * @param int   $iProductId
     *
     * @throws \Bitrix\Main\SystemException
     */
    public function setCachedPrice($fPrice, $iProductId = null)
    {
        if (is_array($this->mixProductId) && is_null($iProductId)) {
            throw new \Exception('mixProductId must be integer or set $iProductId');
        }

        BXClearCache(true, 'local.core/product/'.($iProductId ?? $this->mixProductId).'/price');

        $oCache = \Bitrix\Main\Application::getInstance()
            ->getCache();
        if (
        $oCache->startDataCache(
            60 * 60 * 24 * 365,
            'PRODUCT_PRICE#'.($iProductId ?? $this->mixProductId),
            'local.core/product/'.($iProductId ?? $this->mixProductId).'/price'
        )
        ) {
            $oCache->endDataCache($fPrice);
        }
    }

    /**
     * @param int $iProductId
     *
     * @return float|false
     * @throws \Bitrix\Main\SystemException
     */
    public function getCachedPrice($iProductId = null)
    {
        if (is_array($this->mixProductId) && is_null($iProductId)) {
            throw new \Exception('mixProductId must be integer or set $iProductId');
        }

        $oCache = \Bitrix\Main\Application::getInstance()
            ->getCache();

        if (
        $oCache->initCache(
            60 * 60 * 24 * 365,
            'PRODUCT_PRICE#'.($iProductId ?? $this->mixProductId),
            'local.core/product/'.($iProductId ?? $this->mixProductId).'/price'
        )
        ) {
            return $oCache->getVars();
        }
        return false;
    }
}
