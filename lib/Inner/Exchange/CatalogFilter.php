<?php


namespace Local\Core\Inner\Exchange;

use Bitrix\Iblock\PropertyTable;
use \Local\Core\Assistant\Iblock;
use \Local\Core\Inner\Client\B2BMotion;
use \Local\Core\Model\Data\ProductTypeTable;
use \Local\Core\Model\Data\ProductTypeToFilterTable;

/**
 * Импорт фильтра для каталога
 *
 * @package Local\Core\Inner\Exchange
 */
class CatalogFilter
{
    use \Local\Core\Inner\Trates\Console;

    /**
     * Filter constructor.
     *
     * @param \Symfony\Component\Console\Output\OutputInterface $oOutput
     */
    public function __construct(\Symfony\Component\Console\Output\OutputInterface $oOutput)
    {
        $this->oOutput = $oOutput;
    }


    /** @var array $aPropExtIdToBxId */
    protected $aPropExtIdToBxCode = [];

    /** @var array $aCurrentProductTypesToPropCode */
    protected $aCurrentProductTypesToPropCode = [];

    /** @var array $aProductTypesExtIdToId */
    protected $aProductTypesExtIdToId = [];


    /**
     * Получить новый объект класса для результата
     *
     * @return \stdClass
     */
    protected function getStdResult()
    {
        static $oResult = null;
        if (is_null($oResult)) {
            $oResult = new \stdClass();
            $oResult->id = null;

            $oResult->typesTotal = 0;
            $oResult->typesProcessed = 0;
            $oResult->typesSuccess = 0;
            $oResult->typesError = 0;

            $oResult->propsTotal = 0;
            $oResult->propsProcessed = 0;
            $oResult->propsSkipped = 0;
            $oResult->propsSuccess = 0;
            $oResult->propsError = 0;
            $oResult->propsNotExist = 0;

            $oResult->memoryUsed = 0;
            $oResult->memoryUsedPeak = 0;
            $oResult->isDone = false;
        }

        return $oResult;
    }

    /**
     * Сохранить данные лога по импорту
     *
     * @param        $sEntity
     * @param string $sAction
     *
     * @throws \Exception
     */
    protected function saveStdResult()
    {
        $oStdResult =& $this->getStdResult();
        $oStdResult->memoryUsed = memory_get_usage(true) / 1024 / 1024;
        $oStdResult->memoryUsedPeak = memory_get_peak_usage(true) / 1024 / 1024;
        if ((int)$oStdResult->id > 0) {
            $aData = (array)$oStdResult;
            unset($aData['id']);
            \Local\Core\Model\Data\ImportLogTable::update($oStdResult->id, [
                'ENTITY' => 'catalogFilterSync',
                'ACTION' => 'importResult',
                'DATA' => $aData,
            ]);
        } else {
            $aData = (array)$oStdResult;
            unset($aData['id']);
            $oRes = \Local\Core\Model\Data\ImportLogTable::add([
                'ENTITY' => 'catalogFilterSync',
                'ACTION' => 'importResult',
                'DATA' => $aData,
            ]);
            if ($oRes->isSuccess()) {
                $oStdResult->id = $oRes->getId();
            }
        }
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\Db\SqlQueryException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function execute()
    {
        $this->fillPropsList();
        $this->fillProductTypesToPropCode();
        $this->fillProductTypes();

        if ($this->oOutput->isDebug()) {
            $this->progressBarStart();
            $this->progressBarAddMaxSteps(sizeof($this->aProductTypesExtIdToId));
        }

        $this->getStdResult()->typesTotal = sizeof($this->aProductTypesExtIdToId);
        $this->saveStdResult();

        foreach ($this->aProductTypesExtIdToId as $sProductTypeExtId => $iProductTypeId) {
            $this->getStdResult()->typesProcessed++;
            $oPropsFilter = B2BMotion::getFilterPropsByProductType($sProductTypeExtId);
            if ($oPropsFilter->isSuccess()) {
                $this->getStdResult()->typesSuccess++;
                $this->saveStdResult();

                $aPropsFilterData = $oPropsFilter->getData();
                if (is_array($aPropsFilterData)) {
                    if ($this->oOutput->isDebug()) {
                        $this->progressBarAddMaxSteps(sizeof($aPropsFilterData));
                    }
                    $this->getStdResult()->propsTotal += sizeof($aPropsFilterData);
                    $this->saveStdResult();

                    foreach ($aPropsFilterData as $aProp) {
                        $this->processingProp($sProductTypeExtId, $aProp);
                        if ($this->oOutput->isDebug()) {
                            $this->progressBarAdvance();
                        }
                    }
                }
            } else {
                $this->getStdResult()->typesError++;
                $this->saveStdResult();
            }

            if ($this->oOutput->isDebug()) {
                $this->progressBarAdvance();
            }
        }

        if ($this->oOutput->isDebug()) {
            $this->progressBarFinish();
        }
        $this->getStdResult()->isDone = true;
        $this->saveStdResult();
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function fillPropsList()
    {
        $rsPropExtIdToBxId = \Bitrix\Iblock\PropertyTable::getList([
            'filter' => [
                'IBLOCK_ID' => Iblock\Iblock::getIdByCode('main', 'catalog'),
                '!XML_ID' => false
            ],
            'select' => [
                'CODE',
                'XML_ID'
            ]
        ]);
        while ($a = $rsPropExtIdToBxId->fetch()) {
            $this->aPropExtIdToBxCode[$a['XML_ID']] = $a['CODE'];
        }
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function fillProductTypesToPropCode()
    {
        $rs = ProductTypeToFilterTable::getList([]);
        while ($a = $rs->fetch()) {
            $this->aCurrentProductTypesToPropCode[$a['PRODUCT_TYPE_ID']][$a['PROPERTY_CODE']] = $a['ID'];
        }
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function fillProductTypes()
    {
        $rsProductTypes = ProductTypeTable::getList([
            'select' => [
                'ID',
                'EXTERNAL_ID'
            ]
        ]);
        while ($a = $rsProductTypes->fetch()) {
            $this->aProductTypesExtIdToId[$a['EXTERNAL_ID']] = $a['ID'];
        }
    }

    /**
     * @param $sProductTypeExtId
     * @param $aProp
     *
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    protected function processingProp($sProductTypeExtId, $aProp)
    {
        $this->getStdResult()->propsProcessed++;

        if ((int)$this->aCurrentProductTypesToPropCode[$this->aProductTypesExtIdToId[$sProductTypeExtId]][ $this->aPropExtIdToBxCode[trim($aProp['id'])] ] < 1) {
            if (!empty($this->aPropExtIdToBxCode[trim($aProp['id'])])) {
                $oProductTypeToFilter = ProductTypeToFilterTable::add([
                    'PRODUCT_TYPE_ID' => $this->aProductTypesExtIdToId[$sProductTypeExtId],
                    'PROPERTY_CODE' => $this->aPropExtIdToBxCode[trim($aProp['id'])]
                ]);
                if ($oProductTypeToFilter->isSuccess()) {
                    $this->aCurrentProductTypesToPropCode[$this->aProductTypesExtIdToId[$sProductTypeExtId]][ $this->aPropExtIdToBxCode[trim($aProp['id'])] ] = $oProductTypeToFilter->getId();

                    $arFields = Array('SMART_FILTER' => 'Y', 'IBLOCK_ID' => Iblock\Iblock::getIdByCode('main', 'catalog')) ;
                    $ibp = new \CIBlockProperty();
                    $ibp->Update($oProductTypeToFilter->getId(), $arFields) ;

                    $this->getStdResult()->propsSuccess++;
                } else {
                    $this->getStdResult()->propsError++;
                }
            } else {
                $this->getStdResult()->propsNotExist++;
            }
        } else {
            $this->getStdResult()->propsSkipped++;
        }
        $this->saveStdResult();
    }
}