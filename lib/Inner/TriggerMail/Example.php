<?php


namespace Local\Core\Inner\TriggerMail;


class Example
{
    /**
     * Сообщение об успешной регистрации
     *
     * @param $arFields
     *
     * @return \Bitrix\Main\Entity\AddResult
     */
    public static function exampleTrigger($arFields)
    {
        return \Bitrix\Main\Mail\Event::send(array(
            "EVENT_NAME" => "LOCAL_EXAMPLE_TRIGGER",
            "LID" => "s1",
            "C_FIELDS" => array(
                'EMAIL' => $arFields['EMAIL'],
            )
        ));
    }
}