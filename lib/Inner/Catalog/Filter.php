<?php


namespace Local\Core\Inner\Catalog;

/**
 * Класс для работы с фильтром каталога
 *
 * @package Local\Core\Inner\Catalog
 */
class Filter
{
    /**
     * Получить список св-во для вывода в фильтр по типу изделия.<br/>
     * Возвращает массив символьных кодов св-в.
     *
     * @param int $iProductType ID типа изделия (ID \Local\Core\Model\Data\ProductTypeTable)
     *
     * @return array|false
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getAllowedPropsByProductType($iProductType)
    {
        static $aResult = null;
        if (is_null($aResult)) {
            $rs = \Local\Core\Model\Data\ProductTypeToFilterTable::getList([
                'filter' => [
                    'PRODUCT_TYPE_ID' => $iProductType,
                ],
                'select' => [
                    'PROPERTY_CODE'
                ]
            ]);
            if ($rs->getSelectedRowsCount() > 0) {
                $aResult = $rs->fetchAll();
                $aResult = array_column($aResult, 'PROPERTY_CODE');
                $aResult = array_diff($aResult, [], [''], [null]);
            } else {
                $aResult = false;
            }
        }
        return $aResult;
    }
}