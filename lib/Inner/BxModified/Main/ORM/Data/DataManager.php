<?php

namespace Local\Core\Inner\BxModified\Main\ORM\Data;


class DataManager extends \Bitrix\Main\Entity\DataManager
{
    /**
     * Хранилище значений для полей Enum. Первый ключ - название поля.
     * Пример заполнения:<br/>
     * [<br/>
     * &emsp;'GROUP' => [<br/>
     * &emsp;&emsp;'ECONOM' => 'Экономические единицы',<br/>
     * &emsp;&emsp;'TIME' => 'Единицы времени',<br/>
     * &emsp;&emsp;'LENGTH' => 'Единицы длины',<br/>
     * &emsp;&emsp;'WEIGHT' => 'Единицы массы',<br/>
     * &emsp;&emsp;'VOLUME' => 'Единицы объема',<br/>
     * &emsp;&emsp;'AREA' => 'Единицы площади'<br/>
     * &emsp;]<br/>
     * ]<br/>
     * <br/>
     * Поле ACTIVE заполнено по умолчанию, дублировать не имеет смысла.<br/>
     *
     * @var array $arEnumFieldsValues
     */
    public static $arEnumFieldsValues = [];

    /**
     * Получает значения полей Enum в формате <b>VALUE => TEXT</b>.<br/>
     * Для этого необходимо определить <b>public static $arEnumFieldsValues</b> в ORM.<br/>
     * Пример заполнения $arEnumFieldsValues :<br/>
     * [<br/>
     * &emsp;'GROUP' => [<br/>
     * &emsp;&emsp;'ECONOM' => 'Экономические единицы',<br/>
     * &emsp;&emsp;'TIME' => 'Единицы времени',<br/>
     * &emsp;&emsp;'LENGTH' => 'Единицы длины',<br/>
     * &emsp;&emsp;'WEIGHT' => 'Единицы массы',<br/>
     * &emsp;&emsp;'VOLUME' => 'Единицы объема',<br/>
     * &emsp;&emsp;'AREA' => 'Единицы площади'<br/>
     * &emsp;]<br/>
     * ]<br/>
     * <br/>
     * Поле ACTIVE заполнено по умолчанию, дублировать не имеет смысла.<br/>
     *
     * @param string $strField Код поля
     *
     * @return array
     */
    public static function getEnumFieldHtmlValues($strField)
    {
        $arValues = static::$arEnumFieldsValues[$strField] ?? [];
        if (empty($arValues) && $strField == 'ACTIVE') {
            $arValues = [
                'Y' => 'Да',
                'N' => 'Нет'
            ];
        }

        $arReturn = $arValues;
        return $arReturn;
    }

    /**
     * Получает только значения полей Enum.<br/>
     * Для получения значений и text обращаться к getEnumFieldHtmlValues()<br/>
     * <br/>
     * Поле ACTIVE заполнено по умолчанию, дублировать не имеет смысла.<br/>
     *
     * @param string $strField Код поля
     *
     * @return array
     */
    public static function getEnumFieldValues($strField)
    {
        $arReturn = [];
        if (is_array(static::getEnumFieldHtmlValues($strField))) {
            $arReturn = array_keys(static::getEnumFieldHtmlValues($strField));
        }
        return $arReturn;
    }


    /**
     * Обновим поле DATE_MODIFIED
     *
     * @param \Bitrix\Main\Entity\Event       $event
     * @param \Bitrix\Main\Entity\EventResult $result
     * @param array                        $arModifiedFields
     *
     * @throws \Bitrix\Main\ObjectException
     */
    protected static function _OnBeforeUpdateBase(\Bitrix\Main\Entity\Event &$event, \Bitrix\Main\Entity\EventResult &$result, &$arModifiedFields)
    {

        /** @var \Bitrix\Main\Entity\Event $event */
        $arFields = $event->getParameter('fields');

        if (!empty($arFields)) {
            $arModifiedFields['DATE_MODIFIED'] = new \Bitrix\Main\Type\DateTime();
        }

        $arFields = array_merge($arFields, $arModifiedFields);
        $event->setParameter('fields', $arFields);

        $result->modifyFields($arModifiedFields);
    }
}