<?php

namespace Local\Core\EventHandlers;

use Bitrix\Main\EventManager;

class BaseExemple
{
    /**
     * Регистрирует все обработчики событий
     */
    public static function register()
    {
        self::registerMain();
        self::registerIblock();
	    self::registerSale();
    }


    /**
     * Рестрирует все обработчики событий для модуля main
     */
    private static function registerMain()
    {
        $eventManager = EventManager::getInstance();

//        /** @see \Local\Core\EventHandlers\Main\OnBuildGlobalMenu::addGlobalMenu(); */
//        $eventManager->addEventHandler('main', 'OnBuildGlobalMenu', [Main\OnBuildGlobalMenu::class, 'addGlobalMenu']);

    }

    /**
     * Регистрирует все обработчики событий для модуля iblock
     */
    private static function registerIblock()
    {
	    $eventManager = EventManager::getInstance();
    }

	/**
	 * Регистрирует все обработчики событий для модуля Sale
	 */
	private static function registerSale()
	{
		$eventManager = EventManager::getInstance();
	}

}
