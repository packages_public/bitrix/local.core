<?php

namespace Local\Core\Ajax\Handler;


class Example
{
    public static function example(\Bitrix\Main\HttpRequest $oRequest, \Local\Core\Inner\BxModified\HttpResponse $oResponse, $aRouteParams)
    {
        $aResult = [];

        $aResult['result'] = 'success';
        $aResult['aRouteParams'] = $aRouteParams;
        $aResult['oRequest'] = $oRequest->getPostList()->toArray();


        $oResponse->setContentJson($aResult, 200);
    }
}