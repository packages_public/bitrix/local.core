<?php


namespace Local\Core\Console\Runner;


class DemoConsole
{
    protected $sName = null;

    /** @var \Symfony\Component\Console\Output\Output $oOutput */
    protected $oOutput = null;

    /**
     * DemoConsole constructor.
     *
     * @param \Symfony\Component\Console\Output\Output $oOutput
     */
    public function __construct(\Symfony\Component\Console\Output\Output $oOutput)
    {
        $this->oOutput = $oOutput;
    }

    public function execute()
    {
        $this->oOutput->writeln('Привет, '.$this->getName().'!');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->sName;
    }

    /**
     * @param string $sName
     */
    public function setName(string $sName)
    {
        $this->sName = $sName;
    }
}