<?php

namespace Local\Core\Assistant;

/**
 * Помощник по работе с разделами инфоблоков
 * Class Url
 * @package Local\Core\Assistant\Scalar
 */
class Section
{
    /**
     * Распарс пути умного фильтра
     *
     * @param array $array
     */

    public static function rootCategory($IBLOCK_ID, $IBLOCK_SECTION_ID){

        // получение цепочки разделов
        $scRes = \CIBlockSection::GetNavChain(
            $IBLOCK_ID,
            $IBLOCK_SECTION_ID,
            array("ID","DEPTH_LEVEL")
        );
        while($arGrp = $scRes->Fetch()){
            // определяем корневой раздел
            if ($arGrp['DEPTH_LEVEL'] == 1){
                return $arGrp['ID'];
            }
        }
    }

}
