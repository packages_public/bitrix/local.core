<?php

namespace Local\Core\Assistant;

/**
 * Помощник по работе с URL сайта
 * Class Url
 * @package Local\Core\Assistant\Scalar
 */
class Url
{
    /**
     * Распарс пути умного фильтра
     *
     * @param array $array
     */
    public static function smartFilterPath(){

        $array = explode('/',$_SERVER['REQUEST_URI']);

        $filterKey = array_search('filter', $array);
        $applyKey = array_search('apply', $array);

        if($filterKey){
            $str = '';
            for($i=$filterKey+1; $i<$applyKey; $i++){
                $str .= $array[$i].'/';
            }

            $str = rtrim($str,'/');

            $array = explode('/',$str);

            return $array;
        }
        else{
            return false;
        }
    }

    public static function findProductTypeInUrl($array){

        if(!is_array($array)) return false;

        foreach ($array as $item=>$value){
            $tmp = explode('-is-', $value);
            if($tmp[0] == 'product_type_filter') return mb_strtoupper($tmp[1]);
        }

        return false;

    }

    public static function checkURL($url){
        $urlHeaders = @get_headers($url);
        // проверяем ответ сервера на наличие кода: 200 - ОК
        if(strpos($urlHeaders[0], '200')) {
            return true;
        } else {
            return false;
        }
    }

}
