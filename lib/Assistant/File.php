<?php

namespace Local\Core\Assistant;

use Local\Core\Inner\BxModified\CFile;

/**
 * Помощник по работе с файлами сайта
 * Class Url
 * @package Local\Core\Assistant\Scalar
 */
class File
{

    public static function removeFiles($iblockId = 0, $elementId = 0, $fieldCode) {
        // Получаем данные из БД
        $dbProps = \CIBlockElement::GetProperty($iblockId, $elementId, 'sort', 'asc', ['CODE' => $fieldCode]);
        $arr = [];
        // Разбираем построчно данные из БД
        while ($arItem = $dbProps->Fetch()) {
            // Если указан ключ VALUE, то есть картинка
            if ($arItem['VALUE']) {
                // Так, набираем массив, для удаления
                $arr[$arItem['PROPERTY_VALUE_ID']] = ['VALUE' => ['del' => 'Y']];
                // Не похоже, что без этого фотографии действительно удаляются, лучше проверить
                $res = CFile::Delete($arItem['VALUE']);
            }
        }

        // Одной вилкой чистим множественной свойство картинок
        \CIBlockElement::SetPropertyValueCode(
            $elementId,
            $fieldCode,
            $arr
        );

        return true;

    }

}
