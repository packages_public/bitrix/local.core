<?
namespace Local\Core\Assistant;
use Bitrix\Main\Web\HttpClient;

class GoogleReCaptcha

{

    public static function getPublicKey() { return '6Le_89AZAAAAAKd6DU5Fom6_joW91jnSMwJFSFeB';}

    public static function getSecretKey() { return '6Le_89AZAAAAAMYDRdf9UYiAjaNcXsJJRoHBxeaM';}

    /**

     * @return array - if validation is failed, returns an array with errors, otherwise - empty array. This format is expected by component.

     */

    public static function checkClientResponse()
    {
        $context = \Bitrix\Main\Application::getInstance()->getContext();
        $request = $context->getRequest();
        $captchaResponse = $request->getPost("g-recaptcha-response");
        if($captchaResponse)
        {
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array('secret' => static::getSecretKey(), 'response' => $captchaResponse);
            $httpClient = new HttpClient();
            $response = $httpClient->post($url,$data);

            if($response)
                $response = \Bitrix\Main\Web\Json::decode($response,true);

            if(!$response['success']) {
                return $response['error-codes'][0];
            }
            return array();
        }
        return 'Подтвердите, что Вы не робот';
  }
}