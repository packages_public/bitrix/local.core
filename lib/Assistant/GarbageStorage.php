<?php

namespace Local\Core\Assistant;

/**
 * Помощник для передачи параметров между компонентами
 * Class GarbageStorage
 * @package Local\Core\Assistant\Scalar
 */
class GarbageStorage
{

    private static $storage = array();

    public static function set($name, $value)
    {
        self::$storage[$name] = $value;
    }

    public static function get($name)
    {
        return self::$storage[$name];
    }

}
