<?php

namespace Local\Core\Assistant\Iblock;

use Bitrix\Iblock;
use Local\Core\Inner\BxModified\CFile;
use Local\Core\Inner\Client\B2BMotion;

/**
 * Класс помощник для свойств элементов инфоблока
 *
 * class ElementProperty
 * @package Local\Core\Assistant\Iblock
 */
class ElementProperty
{
    /**
     * Возвращает ID свойства
     *
     * @param string $iblockId - id инфоблока
     * @param string $code - код свойства
     *
     * @return null|int
     */
    public static function getIdByCode(string $iblockId, string $code)
    {
        $iblockId = trim($iblockId);
        $code = trim($code);

        static $arStorage = [];

        if (!isset($arStorage[$iblockId][$code])) {
            $data = Iblock\PropertyTable::getList([
                "select" => ["ID"],
                "filter" => [
                    "=IBLOCK_ID" => $iblockId,
                    "=CODE" => $code,
                ],
                "limit" => 1,
                "cache" => ["ttl" => 86400]
            ])
                ->fetch();

            $arStorage[$iblockId][$code] = $data["ID"] ?? null;
        }

        return $arStorage[$iblockId][$code];
    }

    /**
     * Возвращает перечень значений св-ва типа список
     *
     * @param string $iblockId
     * @param string $code
     *
     * @return mixed
     */
    public static function getEnumerationPropByCode(string $iblockId, string $code)
    {
        $iblockId = trim($iblockId);
        $code = trim($code);

        static $arStorage = [];

        if (!isset($arStorage[$iblockId][$code])) {
            $propertyId = \Local\Core\Assistant\Iblock\ElementProperty::getIdByCode(\Local\Core\Assistant\Iblock\Iblock::getIdByCode('catalog', 'catalog'), $code);
            if ($propertyId) {
                $data = \Bitrix\Iblock\PropertyEnumerationTable::getList([
                    'order' => ['SORT' => 'ASC'],
                    'filter' => ['=PROPERTY_ID' => $propertyId],
                ])
                    ->fetchAll();
            }

            $arStorage[$iblockId][$code] = !empty($data) ? $data : null;
        }

        return $arStorage[$iblockId][$code];
    }

    /**
     *  Функция callback удаляющая пустые свойства из массива товара
     * @param $value
     * @return bool
     */
    protected static function valueNotEmpty($value)
    {
        return $value['VALUE'] == '' ? false : true;
    }

    /**
     * Возвращает свойства и их значения в удобном для перебора виде.
     * @param $iProductID - ID товара
     * @return array - итоговый массив параметров
     */
    public static function getPropProduct($iProductID)
    {

        // Получаем информацию о товаре
        $valueProduct = \CIBlockElement::GetByID($iProductID)->GetNext();

        if ($valueProduct == false) {
            LocalRedirect('/404.php');
        }

        // Получаем информацию о свойствах товара
        $valueProduct['PROP'] = array_filter(\CIBlockElement::GetByID($iProductID)->GetNextElement()->GetProperties(), array(__CLASS__, 'valueNotEmpty'));

        foreach ($valueProduct['PROP'] as $item => $value) {
            if (is_array($value["VALUE"]) and count($value["VALUE"]) <= 1) $valueProduct['PROP'][$item]["VALUE"] = trim($value["VALUE"][0]);
            if ($value["VALUE"] == '0') $valueProduct['PROP'][$item]["VALUE"] = 'Нет';
            if ($value["VALUE"] == '1') $valueProduct['PROP'][$item]["VALUE"] = 'Да';
        }

        // Получаем информацию о бухтовках
        $valueProduct['PROP']['residuePacking'] = \Local\Core\Model\Data\ResiduePackingTable::getList([
            "select" => [
                "*",
            ],
            "filter" => [
                "=EXT_ID" => (string)$valueProduct['PROP']['EXTERNAL_ID']['VALUE']
            ]
        ])->fetchAll();

        // Получаем информацию о документах и сертификатах
        $arDocumentsTmp = \Local\Core\Model\Data\DocumentTable::getList([
            "select" => [
                "*",
            ],
            "filter" => [
                "=EXT_ID" => (string)$valueProduct['PROP']['EXTERNAL_ID']['VALUE']
            ]
        ])->fetchAll();

        foreach ($arDocumentsTmp as $item=>$value){

            $pos = strripos($value['NAME'], 'Сертификат');

            if ($pos === false) {
                $valueProduct['PROP']['Documents'][] = $value;
            } else {
                $valueProduct['PROP']['Certificates'][] = $value;
            }

        }

        // Получаем ссылку на бренд и его название
        $BRAND = \CIBlockElement::GetByID($valueProduct['PROP']['BRAND']['VALUE'])->GetNext();
        unset($valueProduct['PROP']['BRAND']);
        $valueProduct['PROP']['BRAND'] = $BRAND;

        return $valueProduct;

    }
}
