<?php

namespace Local\Core\Assistant;

/**
 * Помощник по работе с BaseOrm Table
 * Class BaseOrm
 * @package Local\Core\Assistant\Scalar
 */
class BaseOrm
{
    /**
     * Получение данных из пользовательского поля
     *
     * @param array $array
     */
    public static function getTableItem($name, $fields = '*', $filter = [], $order = [], $limit = ''){

        return $name::getlist([
            "select" => [
                $fields,
            ],
            "filter" => $filter,
            "order" => $order,
            "limit" => $limit
        ])->fetchAll();

    }

}
