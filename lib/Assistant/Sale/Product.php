<?php

namespace Local\Core\Assistant\Sale;

/**
 * Помощник по работе с товарами в каталоге
 * Class Product
 * @package Local\Core\Assistant\Sale
 */
class Product
{

    public static function getProductByExternalID($EXTERNAL_ID, $arSelect = array("*")){

        if($EXTERNAL_ID == '') return false;

        $arFilter = Array("IBLOCK_ID" => 2, "=PROPERTY_EXTERNAL_ID" => $EXTERNAL_ID);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        $arResult = [];
        while($ob = $res->GetNextElement())
        {
            $arResult[] = array_merge($ob->GetFields(), $ob->GetProperties());
        }

        return $arResult;

    }

    public static function getProductByExternalIDNoProperty($EXTERNAL_ID, $arSelect = array("*")){

        if($EXTERNAL_ID == '') return false;

        $arFilter = Array("IBLOCK_ID" => 2, "=PROPERTY_EXTERNAL_ID" => $EXTERNAL_ID);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
        $arResult = [];
        while($ob = $res->GetNextElement())
        {
            $arResult[] = $ob->GetFields();
        }

        return $arResult;

    }

}
