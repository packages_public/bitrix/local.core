<?php

namespace Local\Core\Assistant\Sale;

use \Bitrix\Main\Loader;
use \Bitrix\Highloadblock as HL;

/**
 * Помощник по работе с заказами
 * Class Order
 * @package Local\Core\Assistant\Sale
 */
class Order
{

	public function get($ID){
		return \Bitrix\Sale\Order::loadByAccountNumber($ID);
	}

	public function getProperties($ID){
		$dbRes = \Bitrix\Sale\PropertyValueCollection::getList([
			'select' => ['*'],
			'filter' => [
				'=ORDER_ID' => $ID,
			]
		]);
		$properties = [];

		while ($item = $dbRes->fetch())
		{
			$properties[] = $item;
		}

		return $properties;
	}

	public function getProperty($ID, $field){
		$dbRes = \Bitrix\Sale\PropertyValueCollection::getList([
			'select' => ['*'],
			'filter' => [
				'=ORDER_ID' => $ID,
				'=CODE' => $field
			]
		]);
		$properties = [];

		while ($item = $dbRes->fetch())
		{
			$properties = $item;
		}

		return $properties;
	}

	public static function getName($ID, $DATE_INSERT_FORMATED){

		return 'RI-' . $DATE_INSERT_FORMATED->format("Y") . '-' . sprintf('%05d', $ID);

	}

	public static function getStatusName($STATUS){


		\Bitrix\Main\Loader::IncludeModule("sale");

		$statusResult = \Bitrix\Sale\Internals\StatusLangTable::getList(array(

			'order' => array('STATUS.SORT'=>'ASC'),

			'filter' => array('STATUS.TYPE'=>'O','LID'=>LANGUAGE_ID, 'STATUS_ID'=>$STATUS),

			'select' => array('NAME','DESCRIPTION'),

		));

		return $statusResult->fetch();

	}

	public static function getProducts($ID){

		$dbBasketItems = \CSaleBasket::GetList(array(), array("ORDER_ID" => $ID), false, false, array());
		while ($arItems = $dbBasketItems->Fetch()) {

			$products[$arItems["ID"]] = $arItems;

			$arSelect = Array("*");
			$arFilter = Array("IBLOCK_ID"=>IntVal(2), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID"=>$arItems["PRODUCT_ID"]);
			$res = \CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$products[$arItems["ID"]]["OFFERS"] = array_merge($ob->GetFields(), $ob->GetProperties());

				/**
				 * @var array Массив описывающий свойство типа справочник
				 */
				$arHighloadProperty = $products[$arItems["ID"]]["OFFERS"]['COLOR_REF'];

				/**
				 * @var string название таблицы справочника
				 */
				$sTableName = $arHighloadProperty['USER_TYPE_SETTINGS']['TABLE_NAME'];

				/**
				 * Работаем только при условии, что
				 *    - модуль highloadblock подключен
				 *    - в описании присутствует таблица
				 *    - есть заполненные значения
				 */
				if ( Loader::IncludeModule('highloadblock') && !empty($sTableName) && !empty($arHighloadProperty["VALUE"]) )
				{
					/**
					 * @var array Описание Highload-блока
					 */
					$hlblock = HL\HighloadBlockTable::getRow([
						'filter' => [
							'=TABLE_NAME' => $sTableName
						],
					]);

					if ( $hlblock )
					{
						/**
						 * Магия highload-блоков компилируем сущность, чтобы мы смогли с ней работать
						 *
						 */
						$entity = HL\HighloadBlockTable::compileEntity( $hlblock );
						$entityClass = $entity->getDataClass();

						$arRecords = $entityClass::getList([
							'filter' => [
								'UF_XML_ID' => $arHighloadProperty["VALUE"]
							],
						]);
						foreach ($arRecords as $record)
						{
							/**
							 * Тут любые преобразования с записью, полученной из таблицы.
							 * Я транслировал почти все напрямую.
							 *
							 * Нужно помнить, что например в UF_FILE возвращается ID файла,
							 * а не полный массив описывающий файл
							 */
							$arRecord = [
								'ID'                  => $record['ID'],
								'UF_NAME'             => $record['UF_NAME'],
								'UF_SORT'             => $record['UF_SORT'],
								'UF_XML_ID'           => $record['UF_XML_ID'],
								'UF_LINK'             => $record['UF_LINK'],
								'UF_DESCRIPTION'      => $record['UF_DESCRIPTION'],
								'UF_FULL_DESCRIPTION' => $record['UF_FULL_DESCRIPTION'],
								'UF_DEF'              => $record['UF_DEF'],
								'UF_FILE'             => [],
								'~UF_FILE'            => $record['UF_FILE'],
							];

							/**
							 * Не очень быстрое решение - сколько записей в инфоблоке, столько файлов и получим
							 * Хорошо было бы вынести под код и там за 1 запрос все получить, а не плодить
							 * по дополнительному запросу на каждый файл
							 */
							if ( !empty($arRecord['~UF_FILE']) )
							{
								$arRecord['UF_FILE'] = \CFile::getById($arRecord['~UF_FILE'])->fetch();
							}

							$products[$arItems["ID"]]["OFFERS"]['COLOR_REF']['EXTRA_VALUE'][$arRecord["UF_XML_ID"]] = $arRecord;
						}
					}
				}

			}

		}

		return $products;

	}

	public static function getPaymentFromOrder($ID){
		// получение списка оплат для заказа 1234
		$dbRes = \Bitrix\Sale\PaymentCollection::getList([
			'select' => ['*'],
			'filter' => [
				'=ORDER_ID' => $ID,
			]
		]);

		return $dbRes->fetch();
	}

	public static function getFullSumProductFromOrder($ID){

		$arProducts = self::getProducts($ID);
		$sum = 0;

		foreach ($arProducts as $key=>$val){
			$sum = $sum + $val["BASE_PRICE"];
		}

		return $sum;
	}

	public static function getDiscountItems($ID){

		$order = \Bitrix\Sale\Order::loadByAccountNumber($ID);
		$discount = $order->getDiscount()->getApplyResult();

		return $discount["ORDER"][0]["RESULT"]["BASKET"];
	}

	public static function getCoupon($ID){

		$couponList = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
			'select' => array('COUPON'),
			'filter' => array('=ORDER_ID' => $ID)
		));

		return $couponList->fetch();
	}

}
