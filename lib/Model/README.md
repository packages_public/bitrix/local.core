## \Local\Core\Model и тонкости ORM

Была проблема, что значения Entity\EnumField везде приходилось прописывать ручками.

Решением проблемы стало унаследование от **\Local\Core\Inner\BxModified\Main\ORM\Data\DataManager**. 

Там определены 2 метода:
+ **public static function getEnumFieldHtmlValues()** - получает коды значений и текст значений Fields\EnumField в формате **VALUE => TEXT** по коду поля.
+ **public static function getEnumFieldValues()** - получает только коды значений Fields\EnumField по коду поля.

Для работы необходимо в описании ORM определить **public static $arEnumFieldsValues**.

Пример описания:
```php
/** @see \Local\Core\Inner\BxModified\Main\ORM\Data\DataManager::$arEnumFieldsValues */
public static $arEnumFieldsValues = [
    'RESOURCE_TYPE' => [
        'LINK' => 'Ссылка на файл',
        'FILE' => 'Загрузить файл',
    ],
    'HTTP_AUTH' => [
        'Y' => 'Да',
        'N' => 'Нет'
    ]
];
```
Значение для **ACTIVE** задано по умолчанию, т.к. для **Model\Data** это обязательно поле!

На текущий момент у нас есть 2 обертки для DataManager в \Local\Core\Model:
+ Data\BaseOrmTable
+ Reference\BaseOrmTable

**Внимание!!!** Название таблиц всегда должны начинаться с **a_**, далее идет **data_** или **reference_**, далее название класса в snake_case без table. Опять же смотри **\Local\Core\Model\Data\UserFavoriteTable**.

----

#### \CLocalCore::getOrmFieldsTable()
У главного класса local.core есть метод **getOrmFieldsTable()**, который позваляет сделать html структуру, описывая **getMap()**.

После любого внесения изменения в **getMap()** и его логические цепочки, необходимо вызвать
```php
echo \CLocalCore::getOrmFieldsTable(\Local\Core\Model\Data\UserFavoriteTable::class);
```
А получившийся текст в ставить описание класса ORM, как это сделано сейчас. Это позволит смотреть поля, их типы и варианты не залезая в код класса. Чисто для PHPDoc'a.

----

#### \CLocalCore::createDBTableByGetMap()

Метод создает таблицу ORM по описанию **getMap()**. **Создавать таблицы надо через него**, не создавать ничего ручками.

Так же еще есть **dropDBTableByGetMap()** и **resetDBTableByGetMap()**

```php
\CLocalCore::createDBTableByGetMap(\Local\Core\Model\Data\UserFavoriteTable::class);
```

---

### \Local\Core\Model\Data

**ORM сущности, которые предназначены для хранение данных по другим сущностям. Данные не должны быть справочными!**

К примеру в **\Local\Core\Model\Data** нужно хранить данные, которые обычно вгоняют в iblock.

Каждый ORM **Model\Data** должен содержать поля **ID**, **ACTIVE**, **DATE_CREATE** и **DATE_MODIFIED**.

Ввиду этого, что бы точно ничего не забыть, необходимо, создавая класс в ORM **Model\Data**, унаследовав от **Model\Data\BaseOrmTable**.

Для большего понимания смотри **\Local\Core\Model\Data\UserFavoriteTable** и изучи **\Local\Core\Model\Data\BaseOrmTable**.

---

### \Local\Core\Model\Reference
**ORM сущности, которые являются справочниками чего либо.**

К примеру справочник единиц измерения. Т.е. то, что обычно вгоняют в highload

Каждый ORM **Model\Reference** должен содержать поля **ID**, **DATE_CREATE**,**DATE_MODIFIED**, **SORT**, **NAME**, **CODE**.

Создавая справочник в ORM **Model\Reference** следует так же унаследоваться от **Model\Reference\BaseOrmTable**.

---