<?php


namespace Local\Core\Model\Data;

use \Bitrix\Main\Entity;


/**
 * Class FavoritesTable
 * Избранное пользователя <br/>
 * <ul><li>ID - ID | Fields\IntegerField</li><li>ACTIVE - Активность [Y] | Fields\EnumField<br/>&emsp;Y => Да<br/>&emsp;N => Нет<br/></li><li>DATE_CREATE - Дата создания [11.10.2020 10:22:22] | Fields\DatetimeField</li><li>DATE_MODIFIED - Дата последнего изменения [11.10.2020 10:22:22] | Fields\DatetimeField</li><li>USER_ID - ID пользователя | Fields\IntegerField</li><li>ELEMENT_ID - ID элемента | Fields\IntegerField</li></ul>
 *
 * @package Local\Core\Model\Data
 */
class FavoritesTable extends BaseOrmTable
{
    public static function getTableName()
    {
        return 'a_data_favorites';
    }

    public static function getMap()
    {
        return array_merge(
            parent::getMap(),
            [
                new Entity\IntegerField('USER_ID', [
                    'title' => 'ID пользователя',
                    'required' => true,
                ]),
                new Entity\IntegerField('ELEMENT_ID', [
                    'required' => true,
                    'title' => 'ID элемента'
                ]),
            ]
        );
    }
}
