<?php

namespace wmstudio\installer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;

class Bitrix extends LibraryInstaller
{

	public function supports($packageType)
	{
		return $packageType === 'composer-plugin';
	}

	public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
	{
		parent::install($repo, $package);
		$name = explode("/", $package->getName());
		$this->initBitrix();
		$module = $this->getModule($name[1]);
		$checkModule = $module->checkModule($name[1]);

		if($checkModule) $module->DoUninstall();

		$module->DoInstall();
	}

	public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
	{
		$name = explode("/", $package->getName());
		$this->initBitrix();
		$module = $this->getModule($name[1]);
		$module->DoUninstall();
		parent::uninstall($repo, $package);
	}

	public function getInstallPath(PackageInterface $package)
	{
		$name = explode("/", $package->getName());
		return "../../local/modules/{$name[1]}/";
	}

	protected function initBitrix()
	{
		$_SERVER['DOCUMENT_ROOT'] = __DIR__ . "/../../../../../";
		define('STOP_STATISTICS', true);
		define("NO_KEEP_STATISTIC", "Y");
		define("NO_AGENT_STATISTIC","Y");
		define("NOT_CHECK_PERMISSIONS", true);
		require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
		$GLOBALS['APPLICATION']->RestartBuffer();
	}

	protected function getModule($module)
	{
		include_once $_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $module . "/install/index.php";
		$class = str_replace(".", "_", $module);
		$module = new $class();
		return $module;
	}

}