<?

class CLocalCore
{
    /**
     * Создает таблицу по ORM.
     *
     * @param \Bitrix\Main\Entity\DataManager $ormClassTable
     *
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    public static function createDBTableByGetMap($ormClassTable)
    {
        if (
        !\Bitrix\Main\Application::getConnection()
            ->isTableExists($ormClassTable::getTableName())
        ) {
            foreach ($ormClassTable::getEntity()
                         ->compileDbTableStructureDump() as $sqlString) {
                $sqlString = str_replace(' NOT NULL', ' ', $sqlString);
                \Bitrix\Main\Application::getConnection()
                    ->query($sqlString);
            }
        }
    }

    /**
     * Удаляет ORM талицум
     *
     * @param \Bitrix\Main\ORM\Data\DataManager $ormClassTable
     *
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    public static function dropDBTableByGetMap($ormClassTable)
    {
        if (
        \Bitrix\Main\Application::getConnection()
            ->isTableExists($ormClassTable::getTableName())
        ) {
            $sqlString = $ormClassTable::getTableName();
            if (!empty($sqlString)) {
                \Bitrix\Main\Application::getConnection()
                    ->dropTable($sqlString);
            }
        }
    }

    /**
     * Удаляет ORM талицу.
     *
     * @param \Bitrix\Main\ORM\Data\DataManager $ormClassTable
     *
     * @throws \Bitrix\Main\Db\SqlQueryException
     */
    public static function resetDBTableByGetMap($ormClassTable)
    {
        if (
        \Bitrix\Main\Application::getConnection()
            ->isTableExists($ormClassTable::getTableName())
        ) {
            static::dropDBTableByGetMap($ormClassTable);
        }
        static::createDBTableByGetMap($ormClassTable);
    }

    /**
     * Добавить класс агента
     *
     * @param string $strAgentClassName Класс агента, унаследованный от \Local\Core\Agent\Base
     * @param int    $intPeriod         Период запуска в секундах
     */
    public static function addAgent($strAgentClassName, $intPeriod = 3600)
    {
        \CAgent::AddAgent($strAgentClassName.'::init();', 'local.core', 'N', $intPeriod, date('d.m.Y H:i:00'), 'N', null, 100);
    }

    /**
     * Возвращает html (htmlspecialchars) описание getMap() ORM, для добалвения в PHPDoc
     *
     * @param string $strClass Полное название класса
     *
     * @return string
     */
    public static function getOrmFieldsTable($strClass)
    {
        $str = '<ul>';
        foreach ($strClass::getMap() as $obField) {
            $str .= '<li>';
            $str .= $obField->getName();
            if ($obField instanceof \Bitrix\Main\ORM\Fields\ScalarField) {
                if (!empty($obField->getTitle())) {
                    $str .= ' - '.$obField->getTitle();
                }

                if (!empty($obField->getDefaultValue())) {
                    $str .= ' ['.$obField->getDefaultValue().']';
                }

                $str .= ' | '.str_replace('Bitrix\Main\ORM\\', '', get_class($obField));

                if ($obField instanceof Bitrix\Main\Entity\EnumField) {
                    try {
                        if (!method_exists($strClass, 'getEnumFieldHtmlValues')) {
                            throw new \Exception();
                        }

                        if (empty($strClass::getEnumFieldHtmlValues($obField->getName()))) {
                            throw new \Exception();
                        }

                        $str .= '<br/>';
                        foreach ($strClass::getEnumFieldHtmlValues($obField->getName()) as $key => $value) {
                            $str .= '&emsp;'.$key.' => '.$value."<br/>";
                        }
                    } catch (\Exception $e) {
                        $str .= '<br/>';
                        foreach ($obField->getValues() as $key => $value) {
                            $str .= '&emsp;'.$value."<br/>";
                        }
                    }
                }

            } else {

                if (!empty($obField->getRefEntityName())) {
                    $str .= ' - '.$obField->getRefEntityName();
                }
                $str .= ' | '.str_replace('Bitrix\Main\ORM\\', '', get_class($obField));

            }

            $str .= '</li>';

        }
        $str .= '</ul>';
        return htmlspecialchars($str);
    }

    /**
     * Функция добавления пункта в меню
     *
     * @param array  $arSubMenu         Массив с меню, к которому добавится пункт
     * @param string $strClassAdminList Полный путь до AdminList
     * @param string $strClassAdminEdit Полный путь до AdminEdit
     * @param string $strName           Название пункта в меню
     * @param string $strIcon           Класс иконки
     */
    public static function addItemToMenu(&$arSubMenu, $strClassAdminList, $strClassAdminEdit, $strName, $strIcon = '')
    {
        if (class_exists($strClassAdminList)) {
            $lDataList = (new $strClassAdminList())->getAdminUri();
            if ($lDataList->isSuccess()) {
                $lDataEdit = (new $strClassAdminEdit())->getAdminUri();

                $arSubMenu[] = [
                    "text" => $strName,
                    'url' => $lDataList->getData()['uri'],
                    "more_url" => ($lDataEdit->isSuccess()) ? [$lDataEdit->getData()["uri"]] : [],
                    "icon" => $strIcon
                ];
            }

        }
    }

    /**
     * Возвращает путь до assets модуля от корня сайта
     *
     * @return mixed
     */
    public static function getModuleAssetsPath()
    {
        return str_replace(\Bitrix\Main\Application::getDocumentRoot(), '', __DIR__.'/assets');
    }

    /**
     * Получить абсолютный путь до модуля
     *
     * @return string
     */
    public static function getAbsoluteModulePath()
    {
        return __DIR__;
    }

    /**
     * Аналог var_export, только приводит к стилю 7.0
     *
     * @param      $expression
     * @param bool $return
     *
     * @return mixed|string|string[]|null
     */
    public static function varExport($expression, $return = false)
    {
        $export = var_export($expression, true);
        $export = preg_replace("/^([ ]*)(.*)/m", '$1$1$2', $export);
        $array = preg_split("/\r\n|\n|\r/", $export);
        $array = preg_replace(["/\s*array\s\($/", "/\)(,)?$/", "/\s=>\s$/"], [null, ']$1', ' => ['], $array);
        $export = join(PHP_EOL, array_filter(["["] + $array));
        if ((bool)$return) {
            return $export;
        } else {
            echo $export;
        }
    }
}